const Sdata = [
    {
        sname:"Dark",
        imgscr:"https://wallpaperaccess.com/full/1605487.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80100172"
    },

    {
        sname: "Money Hiest",
        imgscr:"https://wallpaperaccess.com/full/3932766.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80192098?source=35"
    },

    {
        sname: "The Good Doctor",
        imgscr:"https://wallpaperaccess.com/full/1992035.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80176893?source=35"
    },

    {
        sname: "Sacred Games",
        imgscr:"https://m.media-amazon.com/images/M/MV5BYTMzYTQ1NzAtOGY4OC00ZGM5LThiMmUtMzgzZjgzNzNjZjJmXkEyXkFqcGdeQXVyODk2ODI3MTU@._V1_.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80115328?source=35"
    },

    {
        sname: "You",
        imgscr:"https://m.media-amazon.com/images/M/MV5BMzkzOGFiY2EtZDcyZi00OWNhLThhNmYtYjQ3ODljMTM5ZDBkXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80211991?source=35"
    },
    {
        sname: "The Good Doctor",
        imgscr:"https://wallpaperaccess.com/full/1992035.jpg",
        title:"The Netflix Original Show",
        links:"https://www.netflix.com/in/title/80176893?source=35"
    },
]

export default Sdata;
